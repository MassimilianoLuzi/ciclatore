function relayStatus = CheckRelayStatus(serial2Arduino)
    
    commandCheckRelay = 6;

    serial2Arduino.Terminator = '';
    fprintf(serial2Arduino, '%d', commandCheckRelay);
    serial2Arduino.Terminator = 'LF';
    while ~strcmp(fscanf(serial2Arduino, '%s'), 'ACK')
    end
    relayStatus = fscanf(serial2Arduino, '%d');
end
function relayStatus = ActuateRelay(serial2Arduino, relayCommand, checkFlag)
    serial2Arduino.Terminator = '';
    fprintf(serial2Arduino, '%d', relayCommand);
    
    if checkFlag
        pause(2);
        relayStatus = CheckRelayStatus(serial2Arduino);
    end
end
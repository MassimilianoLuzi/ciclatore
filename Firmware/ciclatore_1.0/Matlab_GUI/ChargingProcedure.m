function complete = ChargingProcedure(serial2Arduino, generator_obj, cRate, maxVoltage, Active_Cell_Vector)

remote_sense = 1;
app_load(load_obj,dropout_load,0,0);
divider = 1;

timer = tic;
while 1
    while ~(mod(toc(timer), 1) < 10^(-4))
    end
    [V, I] = MeasureVI(serial2Arduino, Active_Cell_Vector);
    maxV = max(V);
    if maxV >= maxVoltage
        cRate = cRate/2;
        divider = divider*2;
        if divider == 16
            break
        end
    end
    generator(generator_obj,volt_limit_generator,cRate,1,remote_sense);
end
generator(generator_obj,volt_limit_generator,0,0,remote_sense);
end
cycleFiles = dir(fullfile('*.mat'));

for n=1:length(cycleFiles)
    load(cycleFiles(n).name);
    fid = fopen(strcat('TXT\',info.Cycle,'.txt'),'w');
    for k=1:length(Time)
        fprintf(fid, '%.4f\t%.4f\n', Time(k), C_Rate(k)); 
    end
    fclose(fid);
end
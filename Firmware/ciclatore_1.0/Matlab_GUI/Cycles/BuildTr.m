clear all

Cn =4.4;

Iin = [];
charge_flag = [];

% rest 
Iin = zeros(3600, 1);
charge_flag = zeros(3600, 1);

% NREL2VAIL
load('NREL2VAIL.mat')
Iin = vertcat(Iin, C_Rate*Cn*0.9);
charge_flag = vertcat(charge_flag, zeros(length(C_Rate), 1));

% rest 
Iin = vertcat(Iin, zeros(600, 1));
charge_flag = vertcat(charge_flag, zeros(600, 1));

% Charge
Iin = vertcat(Iin, Cn*ones(3600,1)*0.8);
charge_flag = vertcat(charge_flag, ones(3600, 1));

% rest 
Iin = vertcat(Iin, zeros(600, 1));
charge_flag = vertcat(charge_flag, zeros(600, 1));

% UDDS
load('UDDS.mat');
Iin = vertcat(Iin, C_Rate*Cn);
charge_flag = vertcat(charge_flag, zeros(length(C_Rate), 1));

% US06
load('US06.mat');
Iin = vertcat(Iin, C_Rate*Cn);
charge_flag = vertcat(charge_flag, zeros(length(C_Rate), 1));

% ARB02
load('ARB02.mat');
Iin = vertcat(Iin, C_Rate*Cn);
charge_flag = vertcat(charge_flag, zeros(length(C_Rate), 1));

% rest 
Iin = vertcat(Iin, zeros(600, 1));
charge_flag = vertcat(charge_flag, zeros(600, 1));

cycle_start = 1- charge_flag;

% SoC
SoC(1)=1;
for n=2:length(Iin)
    SoC(n) = SoC(n-1) + Iin(n-1)/(3600*Cn);
end
SoC = SoC(:);

Time = linspace(0,length(Iin)-1, length(Iin))';

plot(SoC)

Iin = [Time, Iin];
charge_flag = [Time, charge_flag];
cycle_start = [Time, cycle_start];
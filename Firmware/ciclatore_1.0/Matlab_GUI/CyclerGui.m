function varargout = CyclerGui(varargin)
% CYCLERGUI MATLAB code for CyclerGui.fig
%      CYCLERGUI, by itself, creates a new CYCLERGUI or raises the existing
%      singleton*.
%
%      H = CYCLERGUI returns the handle to a new CYCLERGUI or the handle to
%      the existing singleton*.
%
%      CYCLERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CYCLERGUI.M with the given input arguments.
%
%      CYCLERGUI('Property','Value',...) creates a new CYCLERGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CyclerGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CyclerGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CyclerGui

% Last Modified by GUIDE v2.5 29-Jan-2018 12:15:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CyclerGui_OpeningFcn, ...
                   'gui_OutputFcn',  @CyclerGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CyclerGui is made visible.
function CyclerGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CyclerGui (see VARARGIN)

% Choose default command line output for CyclerGui
handles.output = hObject;

% handles.Cn = str2num(get(handles.CnData, 'String'));
% handles.cRate = str2num(get(handles.cRateData, 'String'));
% handles.Ts = str2num(get(handles.TsData, 'String'));
% handles.duration = str2num(get(handles.durationData, 'String'));

% close all connections
if ~isempty(instrfind)
    fclose(instrfind);
end

% Initialize profile
handles.Iin_r = {0};
handles.Time_r = {0};
handles.SoC_r = {1};

handles.comStatus = zeros(3, 1);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CyclerGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CyclerGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% close all connections
if ~isempty(instrfind)
    fclose(instrfind);
end

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in cycleSelectMenu.
function cycleSelectMenu_Callback(hObject, eventdata, handles)
% hObject    handle to cycleSelectMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selectedCycleIndex = get(hObject, 'Value');
selectedCycle = hObject.String{selectedCycleIndex};

switch selectedCycle
    case 'Rest'
        set(handles.durationText, 'Enable','on');
        set(handles.durationData, 'Enable','on');

        set(handles.cRateText, 'Enable','off');
        set(handles.cRateData, 'Enable','off');

        set(handles.TsText, 'Enable','on');
        set(handles.TsData, 'Enable','on');
        
    case 'Charging'
        set(handles.durationText, 'Enable','off');
        set(handles.durationData, 'Enable','off');

        set(handles.cRateText, 'Enable','on');
        set(handles.cRateData, 'Enable','on');

        set(handles.TsText, 'Enable','off');
        set(handles.TsData, 'Enable','off');
        
    case 'Constant'
        set(handles.durationText, 'Enable','on');
        set(handles.durationData, 'Enable','on');

        set(handles.cRateText, 'Enable','on');
        set(handles.cRateData, 'Enable','on');

        set(handles.TsText, 'Enable','on');
        set(handles.TsData, 'Enable','on');
    otherwise
        set(handles.durationText, 'Enable','off');
        set(handles.durationData, 'Enable','off');

        set(handles.cRateText, 'Enable','off');
        set(handles.cRateData, 'Enable','off');

        set(handles.TsText, 'Enable','off');
        set(handles.TsData, 'Enable','off');
end
% Hints: contents = cellstr(get(hObject,'String')) returns cycleSelectMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cycleSelectMenu

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function cycleSelectMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cycleSelectMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

cycles = dir(fullfile(strcat(pwd,'/Cycles/', '*.mat')));
cycles = ['Rest', 'Charging', 'Constant', {cycles.name}];
set(hObject, 'Value', 1);
set(hObject, 'String', cycles);


function cRateData_Callback(hObject, eventdata, handles)
% hObject    handle to cRateData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles.cRate = str2num(get(hObject, 'String'));

% Update handles structure
guidata(hObject, handles);

% Hints: get(hObject,'String') returns contents of cRateData as text
%        str2double(get(hObject,'String')) returns contents of cRateData as a double


% --- Executes during object creation, after setting all properties.
function cRateData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cRateData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function durationData_Callback(hObject, eventdata, handles)
% hObject    handle to durationData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of durationData as text
%        str2double(get(hObject,'String')) returns contents of durationData as a double
% handles.duration = str2num(get(hObject, 'String'));

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function durationData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to durationData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TsData_Callback(hObject, eventdata, handles)
% hObject    handle to TsData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TsData as text
%        str2double(get(hObject,'String')) returns contents of TsData as a double
% handles.Ts = str2num(get(hObject, 'String'));

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function TsData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TsData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in addCycleButton.
function addCycleButton_Callback(hObject, eventdata, handles)
% hObject    handle to addCycleButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get Data
selectedCycleIndex = get(handles.cycleSelectMenu, 'Value');
selectedCycleText = get(handles.cycleSelectMenu, 'String');
selectedCycle = selectedCycleText{selectedCycleIndex};

Cn = str2num(get(handles.CnData, 'String'));
cRate = str2num(get(handles.cRateData, 'String'));
duration = str2num(get(handles.durationData, 'String'));
Ts = str2num(get(handles.TsData, 'String'));

% Generate cycle
[Iin_r, Time_r] = AddCycle(selectedCycle, Cn, cRate, duration, Ts);
handles.Iin_r = vertcat(handles.Iin_r, {Iin_r});

% Store new Cycle
handles.Time_r = vertcat(handles.Time_r, {Time_r});

SoC_r = zeros(length(handles.Time_r{end}), 1);
for t=2:length(handles.Time_r{end})
    SoC_r(t) = SoC_r(t-1) + handles.Iin_r{end}(t-1)*(handles.Time_r{end}(t)-handles.Time_r{end}(t-1))/(Cn*3600);
end
handles.SoC_r = vertcat(handles.SoC_r, {SoC_r});

% Update list of cycle
listItem = sprintf('Cycle: %s - %.3f [A] - %d [s]', selectedCycle, cRate, round(Time_r(end)));
listCurrentItems = get(handles.cycleList, 'String');
if strcmp(listCurrentItems, 'None')
    newListValue = listItem;
elseif ~iscell(listCurrentItems)
    newListValue = {listCurrentItems listItem};
else
    newListValue = {listCurrentItems{:} listItem};
end
set(handles.cycleList, 'String', newListValue);

% Update handles structure
guidata(hObject, handles);

Time = handles.Time_r{1};
Iin = handles.Iin_r{1};
SoC = handles.SoC_r{1};
for n = 1:length(handles.Time_r)
    if isnan(handles.Time_r{n})
        Time = vertcat(Time, linspace(1,3600,3600)' + Time(end));
        Iin = vertcat(Iin, handles.Iin_r{n}*ones(3600,1));
        SoC = vertcat(SoC, ones(3600,1));
    else
        Time = vertcat(Time, handles.Time_r{n}+Time(end));
        Iin = vertcat(Iin, handles.Iin_r{n});
        SoC = vertcat(SoC, handles.SoC_r{n}+SoC(end));
    end
end

% Plot current cycle
axes(handles.profilePlot);
yyaxis left
plot(Time/3600, Iin)
yyaxis right
plot(Time/3600, SoC);

title('Input Current and SoC');
xlabel('Time [h]');
yyaxis left
ylabel('Current [A]');
yyaxis right
ylabel('SoC [%]');



% --- Executes on button press in deleteCycle.
function deleteCycle_Callback(hObject, eventdata, handles)
% hObject    handle to deleteCycle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get Data
Cn = str2num(get(handles.CnData, 'String'));

listCurrentItems = get(handles.cycleList, 'String');
if sum(strcmp(listCurrentItems, 'None')) || isempty(listCurrentItems)
    msgbox('Nothing to delete');
else
    selectedListIndex = get(handles.cycleList, 'Value');
    selectedCycleIndex = selectedListIndex + 1;

    % Update list cycles
    listCurrentItems{selectedListIndex} = [];
    listCurrentItems(cellfun(@(listCurrentItems) isempty(listCurrentItems),listCurrentItems))=[];
    set(handles.cycleList, 'String', listCurrentItems);
    set(handles.cycleList, 'Value', 1);
    
    % Delete selcted cycle from the cell vector
    Time_r = handles.Time_r;
    Time_r{selectedCycleIndex} = [];
    Time_r(cellfun(@(Time_r) isempty(Time_r),Time_r))=[];
    handles.Time_r = Time_r;

    Iin_r = handles.Iin_r;
    Iin_r{selectedCycleIndex} = [];
    Iin_r(cellfun(@(Iin_r) isempty(Iin_r),Iin_r))=[];
    handles.Iin_r = Iin_r;
    
    SoC_r = handles.SoC_r;
    SoC_r{selectedCycleIndex} = [];
    SoC_r(cellfun(@(SoC_r) isempty(SoC_r),SoC_r))=[];
    handles.SoC_r = SoC_r;

    % Update handles structure
    guidata(hObject, handles);

    Time = handles.Time_r{1};
    Iin = handles.Iin_r{1};
    SoC = handles.SoC_r{1};
    for n = 1:length(handles.Time_r)
        if isnan(handles.Time_r{n})
            Time = vertcat(Time, linspace(1,3600,3600)' + Time(end));
            Iin = vertcat(Iin, handles.Iin_r{n}*ones(3600,1));
            SoC = vertcat(SoC, ones(3600,1));
        else
            Time = vertcat(Time, handles.Time_r{n}+Time(end));
            Iin = vertcat(Iin, handles.Iin_r{n});
            SoC = vertcat(SoC, handles.SoC_r{n}+SoC(end));
        end
    end

    % Plot current cycle
    axes(handles.profilePlot);
    yyaxis left
    plot(Time/3600, Iin)
    yyaxis right
    plot(Time/3600, SoC);

    title('Input Current and SoC');
    xlabel('Time [h]');
    yyaxis left
    ylabel('Current [A]');
    yyaxis right
    ylabel('SoC [%]');
end

% --- Executes on button press in resetButton.
function resetButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Iin_r = {0};
handles.Time_r = {0};
handles.SoC_r = {1};

set(handles.cycleList, 'String', 'None');
% Update handles structure
guidata(hObject, handles);

axes(handles.profilePlot);
yyaxis left
cla;
yyaxis right
cla;

% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%acquisitionJob = batch(@TestProcedure, 3, {handles.time, handles.currentProfile, handles.outVoltagePlot, handles.serial2Arduino});
%data = parfeval(@TestProcedure, 3, handles.time, handles.currentProfile, handles.outVoltagePlot, handles.serial2Arduino);

volt_limit_generator = str2double(get(handles.voltLimitGenerator, 'String'));
dropout_load = str2double(get(handles.dropoutLoad, 'String'));

% 0 = not active, 1 = active; 1 = <, 2 = >, 3 = =
% Vector stop with six entrees:1. 1/0 2. </>/= 3. voltage_stop_value
% 4. 1/0 5. </>/= 6.current_stop_value
stop=[0 0 0 0 0 0];
stop(1)= (get(handles.voltage_stop_cond,'Value'));
selIndex = get(handles.voltage_stop_cond_op,'Value');
opValues = get(handles.voltage_stop_cond_op,'String');
if strcmp(opValues{selIndex}, '<')
stop(2)=1;
elseif strcmp(opValues{selIndex}, '>')
stop(2)=2;
else
stop(2)=3;
end
stop(3)=str2double(get(handles.voltage_stop_cond_value, 'String'));

stop(4)= (get(handles.current_stop_cond,'Value'));
selIndex = get(handles.current_stop_cond_op,'Value');
opValues = get(handles.current_stop_cond_op,'String');
if strcmp(opValues{selIndex}, '<')
stop(5)=1;
elseif strcmp(opValues{selIndex}, '>')
stop(5)=2;
else
stop(5)=3;
end
stop(6)=str2double(get(handles.current_stop_cond_value, 'String'));

Active_Cell_Vector = logical([get(handles.cell_active_1,'Value'),get(handles.cell_active_2,'Value'),get(handles.cell_active_3,'Value'),get(handles.cell_active_4,'Value'),get(handles.cell_active_5,'Value'),get(handles.cell_active_6,'Value'),get(handles.cell_active_7,'Value'),get(handles.cell_active_8,'Value'),get(handles.cell_active_9,'Value'),get(handles.cell_active_10,'Value'),get(handles.cell_active_11,'Value'),get(handles.cell_active_12,'Value')]);

assignin('base', 'stopFlag', false);
if isfield(handles, 'serial2Arduino') && isfield(handles, 'generator_obj') && isfield(handles, 'load_obj')
   if strcmp(handles.serial2Arduino.Status, 'open') && strcmp(handles.generator_obj.Status, 'open') && strcmp(handles.load_obj.Status, 'open')
       relayStatus = CheckRelayStatus(handles.serial2Arduino);
       if relayStatus
           doTestFlag  = 1;
       else
           answer  = questdlg('The relay is open.  Would you like to continue?', 'Warning');
           if strcmp(answer, 'Yes')
               doTestFlag  = 1;
           else
               doTestFlag  = 0;
           end
       end
       if doTestFlag
           [handles.Vout, handles.Iin, handles.Time] = PerformTest(handles.Time_r, handles.Iin_r, Active_Cell_Vector, ...
               volt_limit_generator, dropout_load, ...
               handles.serial2Arduino, handles.generator_obj, handles.load_obj, ...
               handles.outVoltagePlot, handles.profilePlot, handles.statusLabel);
       end
   else
       msgbox('Communication objects must be open');
   end
else
    msgbox('Communication objects must be created');
end
relayStatus  = ActuateRelay(handles.serial2Arduino, 0, 1);

if ~relayStatus
    set(handles.RealyStatus,'ForegroundColor', 'red');
    set(handles.RealyStatus,'String', 'Relay Status: off');
    set(handles.RelayButton, 'Value', 0);
    set(handles.RelayButton,'BackgroundColor','red');
end

assignin('base', 'stopFlag', false);                                 
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignin('base', 'stopFlag', true);

% --- Executes on selection change in cycleList.
function cycleList_Callback(hObject, eventdata, handles)
% hObject    handle to cycleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cycleList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cycleList


% --- Executes during object creation, after setting all properties.
function cycleList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cycleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function comPortData_Callback(hObject, eventdata, handles)
% hObject    handle to comPortData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of comPortData as text
%        str2double(get(hObject,'String')) returns contents of comPortData as a double


% --- Executes during object creation, after setting all properties.
function comPortData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to comPortData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

serialInfo = instrhwinfo('serial');
portsName = serialInfo.AvailableSerialPorts;

if ~isempty(portsName)
    set(hObject, 'String', portsName);
else
    set(hObject, 'String', 'None');
end


function baudRateData_Callback(hObject, eventdata, handles)
% hObject    handle to baudRateData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of baudRateData as text
%        str2double(get(hObject,'String')) returns contents of baudRateData as a double


% --- Executes during object creation, after setting all properties.
function baudRateData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to baudRateData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

delete(hObject);



function CnData_Callback(hObject, eventdata, handles)
% hObject    handle to CnData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.Cn = str2num(get(hObject, 'String'));

% Hints: get(hObject,'String') returns contents of CnData as text
%        str2double(get(hObject,'String')) returns contents of CnData as a double

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function CnData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CnData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function profilePlot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to profilePlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

axes(hObject);

title('Input Current and SoC');
xlabel('Time [h]');
yyaxis left
ylabel('Current [A]');
yyaxis right
ylabel('SoC [%]');

% 
% guidata(hObject, handles);
% Hint: place code in OpeningFcn to populate profilePlot


% --- Executes during object creation, after setting all properties.
function outVoltagePlot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outVoltagePlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axes(hObject);

title('Measured Voltage');
xlabel('Time [h]');
ylabel('Voltage [V]');
% Hint: place code in OpeningFcn to populate outVoltagePlot


% --- Executes on selection change in G_address.
function G_address_Callback(hObject, eventdata, handles)
% hObject    handle to G_address (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns G_address contents as cell array
%        contents{get(hObject,'Value')} returns selected item from G_address


% --- Executes during object creation, after setting all properties.
function G_address_CreateFcn(hObject, eventdata, handles)
% hObject    handle to G_address (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function G_port_Callback(hObject, eventdata, handles)
% hObject    handle to G_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of G_port as text
%        str2double(get(hObject,'String')) returns contents of G_port as a double


% --- Executes during object creation, after setting all properties.
function G_port_CreateFcn(hObject, eventdata, handles)
% hObject    handle to G_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in L_address.
function L_address_Callback(hObject, eventdata, handles)
% hObject    handle to L_address (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns L_address contents as cell array
%        contents{get(hObject,'Value')} returns selected item from L_address


% --- Executes during object creation, after setting all properties.
function L_address_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_address (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_port_Callback(hObject, eventdata, handles)
% hObject    handle to L_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L_port as text
%        str2double(get(hObject,'String')) returns contents of L_port as a double


% --- Executes during object creation, after setting all properties.
function L_port_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_port (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in comButton.
% function comButton_Callback(hObject, eventdata, handles)
% hObject    handle to comButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function voltLimitGenerator_Callback(hObject, eventdata, handles)
% hObject    handle to voltLimitGenerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of voltLimitGenerator as text
%        str2double(get(hObject,'String')) returns contents of voltLimitGenerator as a double


% --- Executes during object creation, after setting all properties.
function voltLimitGenerator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to voltLimitGenerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dropoutLoad_Callback(hObject, eventdata, handles)
% hObject    handle to dropoutLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dropoutLoad as text
%        str2double(get(hObject,'String')) returns contents of dropoutLoad as a double



% --- Executes during object creation, after setting all properties.
function dropoutLoad_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dropoutLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function comButton_Callback(hObject, eventdata, handles)
% hObject    handle to voltLimitGenerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of voltLimitGenerator as text
%        str2double(get(hObject,'String')) returns contents of voltLimitGenerator as a double
if sum(handles.comStatus) == 0
    A_com = get(handles.comPortData,'String');
    baudRateList = get(handles.baudRateData,'String');
    selectedIndex = get(handles.baudRateData,'Value');
    A_baudRate = baudRateList{selectedIndex};
    handles.serial2Arduino = connect_arduino(A_com,A_baudRate);

    G_ad = get(handles.G_address,'String');
    G_p = get(handles.G_port,'String');
    handles.generator_obj = connect_generator(G_ad,G_p);

    L_ad = get(handles.L_address,'String');
    L_p = get(handles.L_port,'String');
    handles.load_obj = connect_load(L_ad,L_p);

    handles.comStatus = TestCommunications(handles.serial2Arduino, handles.generator_obj, handles.load_obj);

    if handles.comStatus(1)
        set(handles.state_arduino, 'String', 'OK','ForegroundColor', [0, 1, 0]);
    else
        set(handles.state_arduino, 'String', 'Fail','ForegroundColor', [1, 0, 0]);
    end

    if handles.comStatus(2)
        set(handles.state_generator, 'String', 'OK','ForegroundColor', [0, 1, 0]);
    else
        set(handles.state_generator, 'String', 'Fail','ForegroundColor', [1, 0, 0]);
    end

    if handles.comStatus(3)
        set(handles.state_load, 'String', 'OK','ForegroundColor', [0, 1, 0]);
    else
        set(handles.state_load, 'String', 'Fail','ForegroundColor', [1, 0, 0]);
    end
    
    set(handles.comButton, 'String', 'Disconnenct from all devices');
else
    fclose(instrfind);
    
    handles.comStatus = zeros(3, 1);
    
    set(handles.state_arduino, 'String', 'Communication Not Verified','ForegroundColor', [1, 0, 0]);
    set(handles.state_generator, 'String', 'Communication Not Verified','ForegroundColor', [1, 0, 0]);
    set(handles.state_load, 'String', 'Communication Not Verified','ForegroundColor', [1, 0, 0]);
    
    set(handles.comButton, 'String', 'Connenct to all devices');
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in cell_active_1.
function cell_active_1_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_1,'Value'))==1
    set(handles.cell_active_1,'BackgroundColor','green')
else
    set(handles.cell_active_1,'BackgroundColor','red')
end




% --- Executes on button press in cell_active_2.
function cell_active_2_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_2,'Value'))==1
    set(handles.cell_active_2,'BackgroundColor','green')
else
    set(handles.cell_active_2,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_3.
function cell_active_3_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_3,'Value'))==1
    set(handles.cell_active_3,'BackgroundColor','green')
else
    set(handles.cell_active_3,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_4.
function cell_active_4_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_4,'Value'))==1
    set(handles.cell_active_4,'BackgroundColor','green')
else
    set(handles.cell_active_4,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_5.
function cell_active_5_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_5,'Value'))==1
    set(handles.cell_active_5,'BackgroundColor','green')
else
    set(handles.cell_active_5,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_6.
function cell_active_6_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_6,'Value'))==1
    set(handles.cell_active_6,'BackgroundColor','green')
else
    set(handles.cell_active_6,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_7.
function cell_active_7_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_7,'Value'))==1
    set(handles.cell_active_7,'BackgroundColor','green')
else
    set(handles.cell_active_7,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_8.
function cell_active_8_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_8,'Value'))==1
    set(handles.cell_active_8,'BackgroundColor','green')
else
    set(handles.cell_active_8,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_9.
function cell_active_9_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_9,'Value'))==1
    set(handles.cell_active_9,'BackgroundColor','green')
else
    set(handles.cell_active_9,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_10.
function cell_active_10_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_10,'Value'))==1
    set(handles.cell_active_10,'BackgroundColor','green')
else
    set(handles.cell_active_10,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_11.
function cell_active_11_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_11,'Value'))==1
    set(handles.cell_active_11,'BackgroundColor','green')
else
    set(handles.cell_active_11,'BackgroundColor','red')
end

% --- Executes on button press in cell_active_12.
function cell_active_12_Callback(hObject, eventdata, handles)
% hObject    handle to cell_active_12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.cell_active_12,'Value'))==1
    set(handles.cell_active_12,'BackgroundColor','green')
else
    set(handles.cell_active_12,'BackgroundColor','red')
end


% --- Executes on button press in voltage_stop_cond.
function voltage_stop_cond_Callback(hObject, eventdata, handles)
% hObject    handle to voltage_stop_cond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of voltage_stop_cond


% --- Executes on selection change in voltage_stop_cond_op.
function voltage_stop_cond_op_Callback(hObject, eventdata, handles)
% hObject    handle to voltage_stop_cond_op (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns voltage_stop_cond_op contents as cell array
%        contents{get(hObject,'Value')} returns selected item from voltage_stop_cond_op


% --- Executes during object creation, after setting all properties.
function voltage_stop_cond_op_CreateFcn(hObject, eventdata, handles)
% hObject    handle to voltage_stop_cond_op (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in current_stop_cond.
function current_stop_cond_Callback(hObject, eventdata, handles)
% hObject    handle to current_stop_cond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of current_stop_cond


% --- Executes on selection change in current_stop_cond_op.
function current_stop_cond_op_Callback(hObject, eventdata, handles)
% hObject    handle to current_stop_cond_op (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns current_stop_cond_op contents as cell array
%        contents{get(hObject,'Value')} returns selected item from current_stop_cond_op


% --- Executes during object creation, after setting all properties.
function current_stop_cond_op_CreateFcn(hObject, eventdata, handles)
% hObject    handle to current_stop_cond_op (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function voltage_stop_cond_value_Callback(hObject, eventdata, handles)
% hObject    handle to voltage_stop_cond_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of voltage_stop_cond_value as text
%        str2double(get(hObject,'String')) returns contents of voltage_stop_cond_value as a double


% --- Executes during object creation, after setting all properties.
function voltage_stop_cond_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to voltage_stop_cond_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function current_stop_cond_value_Callback(hObject, eventdata, handles)
% hObject    handle to current_stop_cond_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of current_stop_cond_value as text
%        str2double(get(hObject,'String')) returns contents of current_stop_cond_value as a double


% --- Executes during object creation, after setting all properties.
function current_stop_cond_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to current_stop_cond_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in RelayButton.
function RelayButton_Callback(hObject, eventdata, handles)
% hObject    handle to RelayButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles, 'serial2Arduino')
   if strcmp(handles.serial2Arduino.Status, 'open')
       commandReleOn = 1;
       commandReleOff = 0;
       
        buttonStatus = get(hObject, 'Value');
        if buttonStatus
            relayStatus = ActuateRelay(handles.serial2Arduino, commandReleOn, 1);
            set(hObject,'BackgroundColor','green');
        else
            relayStatus = ActuateRelay(handles.serial2Arduino, commandReleOff, 1);
            set(hObject, 'Value', 0);
            set(hObject,'BackgroundColor','red');
        end
        
        if relayStatus
            set(handles.RealyStatus,'ForegroundColor', 'green');
            set(handles.RealyStatus,'String', 'Relay Status: on');
        else
            set(handles.RealyStatus,'ForegroundColor', 'red');
            set(handles.RealyStatus,'String', 'Relay Status: off');
        end
   else
       msgbox('Serial to Arduino must be open');
       set(hObject, 'Value', 0);
       set(hObject,'BackgroundColor','red');
   end
else
    msgbox('Serial to Arduino must be created');
    set(hObject, 'Value', 0);
    set(hObject,'BackgroundColor','red');
end
% Hint: get(hObject,'Value') returns toggle state of RelayButton


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over RealyStatus.
function RealyStatus_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to RealyStatus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'serial2Arduino')
   if strcmp(handles.serial2Arduino.Status, 'open')
        relayStatus = CheckRelayStatus(handles.serial2Arduino);
        if relayStatus
            set(hObject,'ForegroundColor', 'green');
            set(hObject,'String', 'Relay Status: on');
        else
            set(hObject,'ForegroundColor', 'red');
            set(hObject,'String', 'Relay Status: off');
        end
   else
       msgbox('Serial to Arduino must be open');
       set(hObject,'ForegroundColor', 'red');
       set(hObject,'String', 'Relay Status: off');
   end
else
    msgbox('Serial to Arduino must be created');
    set(hObject,'ForegroundColor', 'red');
    set(hObject,'String', 'Relay Status: off');
end





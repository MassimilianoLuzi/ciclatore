function  serial2Arduino = connect_arduino(comPort,baudRate_data)

baudRatedata_num=str2double(baudRate_data);
serial2Arduino = serial(comPort, 'BaudRate', baudRatedata_num);
% serial2Arduino.Terminator = '';

fclose(serial2Arduino);
fopen(serial2Arduino);
flushinput(serial2Arduino);
pause(10e-3);



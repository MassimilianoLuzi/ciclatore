function [Vout, Iin, Time] = PerformTest(Time_r, Iin_r, Active_Cell_Vector, volt_limit_generator, dropout_load, serial2Arduino, generator_obj, load_obj, figureVoltAxes, figureAmpAxes, stateLabel)

commandReleOff = 0;
commandReleOn = 1;

numCycles=length(Time_r);

Time_plot = Time_r{1};
Iin_plot = Iin_r{1};
for n = 2:numCycles
    Time_plot = vertcat(Time_plot, Time_r{n}+Time_plot(end));
    Iin_plot = vertcat(Iin_plot, Iin_r{n});
end
totalTime = Time_plot(end);

stopFlag  = false;

[V, I]= MeasureVI(serial2Arduino, Active_Cell_Vector);
Time = 0;
Vout = V;
Iin = I;

remote_sense=1;

timer = tic;
n=2;
while n <= numCycles
    if evalin('base', 'stopFlag')
        stopFlag  = true;
        break;
    end
    
    t=1;
    chargingFlag = sum(isnan(Time_r{n}));
    if chargingFlag
        divider = 1;
        cRate = Iin_r{n};
    end
    while t <= length(Time_r{n}) || chargingFlag
        if evalin('base', 'stopFlag')
            stopFlag  = true;
            break;
        end
        statusLabel = sprintf('Running \t Cycle: %d of %d \t Cycle Completed: %.2f %%', n-1, numCycles-1, 100*(t-1)/length(Time_r{n}));
        set(stateLabel, 'String', statusLabel, 'ForegroundColor', 'Green');
        
        if chargingFlag
            while ~(mod(toc(timer), 1) < 10^(-4))
            end
            
            maxV = max(Vout(end,:));
            if maxV >= volt_limit_generator
                cRate = cRate/2;
                divider = divider*2;
                if divider == 32
                    chargingFlag = false;
                    break;
                end
            end
            generator(generator_obj,volt_limit_generator+0.01,cRate,1,remote_sense);
        else
            if t==1
                while ~(mod(toc(timer), Time_r{n}(t)-0) < 10^(-4))
                end
            else
                while ~(mod(toc(timer), Time_r{n}(t)-Time_r{n}(t-1)) < 10^(-4))
                end
            end

            if Iin_r{n}(t)>0
                app_load(load_obj,dropout_load,0,0);
                pause(1e-3);
                generator(generator_obj,volt_limit_generator,Iin_r{n}(t),1,remote_sense);
            elseif Iin_r{n}(t)<0
                generator(generator_obj,volt_limit_generator,0,0,remote_sense);
                pause(1e-3);
                app_load(load_obj,dropout_load,-Iin_r{n}(t),1);
            else
                generator(generator_obj,volt_limit_generator,0,0,remote_sense);
                pause(1e-3);
                app_load(load_obj,dropout_load,0,0);
            end
            t=t+1;
        end
    

        [V, I]= MeasureVI(serial2Arduino, Active_Cell_Vector);
        Vout = vertcat(Vout, V);
        Iin  = vertcat(Iin, I);
        Time = vertcat(Time, toc(timer));

        assignin('base', 'Time', Time);
        assignin('base', 'Vout', Vout);
        assignin('base', 'Iin', Iin);

        axes(figureVoltAxes);
        plot(Time/3600, Vout);
        title('Measured Voltage');
        xlabel('Time [h]');
        ylabel('Voltage [V]');
        drawnow;

        axes(figureAmpAxes);
        yyaxis right
        cla;
        ylabel('');
        yyaxis left
        cla;
        %plot(Time_plot(1:length(Iin))/3600, Iin_plot(1:length(Iin)), 'b' , Time/3600, Iin, 'r');
%         plot(Time(2:end)/3600, Vr1(2:end), Time(2:end)/3600, Vr2(2:end));
        plot(Time(2:end)/3600, Iin(2:end));
        title('Measured Current');
        xlabel('Time [h]'); 
        ylabel('Current [A]');
        drawnow;
    end
    n=n+1;
    
    if evalin('base', 'stopFlag')
        stopFlag  = true;
        break;
    end
end


pause(1);
generator(generator_obj,volt_limit_generator,0,0,remote_sense);
app_load(load_obj,dropout_load,0,0);

if stopFlag
    statusLabel = sprintf('Stopped by user during cycle %d of %d', n-2, numCycles-1);
    set(stateLabel, 'String', statusLabel, 'ForegroundColor', 'Red');
else
    statusLabel = sprintf('Complete');
    set(stateLabel, 'String', statusLabel, 'ForegroundColor', 'Green');
end

answer  = questdlg('Test Completed. Would you like to save the measurements?', 'Completed');
if strcmp(answer, 'Yes')
   [fileName, filePath] = uiputfile('Save  Measurements', 'Save Measurements');
   save(strcat(filePath, fileName), 'Time',  'Iin', 'Vout');
end

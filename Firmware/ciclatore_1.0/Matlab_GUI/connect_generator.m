function [generator_obj, status] = connect_generator(address,port)

%Connecting to QPX1200SP:

% Find a tcpip object.
port_num=str2double(port);
generator_obj= instrfind('Type', 'tcpip', 'RemoteHost', address, 'RemotePort', port_num, 'Tag', '');
% Create the tcpip object if it does not exist
% otherwise use the object that was found.
if isempty(generator_obj)
    generator_obj = tcpip(address, port_num);
else
    fclose(generator_obj);
    generator_obj = generator_obj(1);
end
set(generator_obj, 'InputBufferSize', 32);
set(generator_obj, 'OutputBufferSize', 32);
set(generator_obj, 'TimerPeriod', 1)
set(generator_obj, 'TransferDelay', 'on');
% Disconnect from instrument object, obj1.
fclose(generator_obj);
% Connect to instrument object, obj1.
fopen(generator_obj);




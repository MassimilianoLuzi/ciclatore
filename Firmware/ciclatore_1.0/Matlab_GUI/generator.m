
function [voltage_generator,current_generator,operate_va,remote_sense_va] = generator(generator_obj,volt_limit,current_contr,operate,remote_sense)


% Communicating with instrument object, obj1.


fprintf(generator_obj,'V1 %2.3f',volt_limit);
pause(1e-3);
fprintf(generator_obj,'I1 %2.3f',current_contr);
pause(1e-3);
fprintf(generator_obj,'OP1 %1.0f',operate);
pause(1e-3);
fprintf(generator_obj,'SENSE1 %1.0f',remote_sense);
pause(1e-3);

% voltage_generator = query(generator_obj, 'V1O?');
% pause(1e-3);
% current_generator =  query(generator_obj, 'I1O?');
% pause(1e-3);
% operate_va = query(generator_obj, 'OP1?');
% remote_sense_va = remote_sense;

 



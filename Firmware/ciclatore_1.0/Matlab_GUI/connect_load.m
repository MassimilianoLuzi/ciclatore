function [load_obj, status] = connect_load(address, port)

%Connecting to LP400P

% Find a tcpip object.
port_num=str2double(port);
load_obj = instrfind('Type', 'tcpip', 'RemoteHost', address, 'RemotePort', port_num, 'Tag', '');
% Create the tcpip object if it does not exist
% otherwise use the object that was found.
if isempty(load_obj)
    load_obj = tcpip(address, port_num);
else
    fclose(load_obj);
    load_obj = load_obj(1);
end
set(load_obj, 'InputBufferSize', 16);
set(load_obj, 'OutputBufferSize', 16);
set(load_obj, 'TimerPeriod', 0.01);
set(load_obj, 'Timeout', 0.01);
set(load_obj, 'TransferDelay', 'off');

% Disconnect from instrument object, obj2.
fclose(load_obj);
% Connect to instrument object, obj2.
fopen(load_obj);
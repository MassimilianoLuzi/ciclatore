function [Vout, Iin] = MeasureVI(serial2Arduino, Active_Cell_Vector)
    
    commandMeasure = 5;

    serial2Arduino.Terminator = '';
    fprintf(serial2Arduino, '%d', commandMeasure);
    serial2Arduino.Terminator = 'LF';
    while ~strcmp(fscanf(serial2Arduino, '%s'), 'ACK')
    end
    V_read = fscanf(serial2Arduino);
    I_read = fscanf(serial2Arduino);

    V = strsplit(V_read, ',');
    I = strsplit(I_read, ',');

    V = str2num(vertcat(V{1:end-1}));
    I = str2num(vertcat(I{1:end-1}));

    Vout = V(Active_Cell_Vector)';
    Iin  = (I(3)-2.5098)*45.2;
end
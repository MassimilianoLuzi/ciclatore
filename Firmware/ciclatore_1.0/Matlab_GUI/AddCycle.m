function [Iin, Time] = AddCycle( cycleType, Cn, cRate, duration, Ts)
% AddCycle
%   This function add the selected cycle to the testing list

switch cycleType
    case 'Charging'
        C_Rate = cRate;
        Time = nan;
        
    case 'Constant'
        Time = linspace(Ts, duration, duration/Ts)';
        C_Rate = cRate*ones(duration/Ts, 1);
        
    case 'Rest'
        Time = linspace(Ts, duration, duration/Ts)';
        C_Rate = zeros(duration/Ts, 1);
      
    otherwise
        cycles = dir(fullfile(strcat(pwd,'/Cycles/', '*.mat')));
        load(strcat(pwd,'/Cycles/', cycleType));
        Time = Time+1;
end

Iin = C_Rate*Cn;

end
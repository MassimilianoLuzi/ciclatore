function comStatus = TestCommunications(serial2Arduino, generator_obj, load_obj)

% Test Arduino:
pause(5);
fprintf(serial2Arduino, '%d', 21);
pause(1);
test_arduino_1 = fscanf(serial2Arduino, '%d');
pause(1e-3);
fprintf(serial2Arduino, '%d', 20);
pause(1e-3);
test_arduino_2 = fscanf(serial2Arduino, '%d');

comStatus(1) = test_arduino_1 == 1 && test_arduino_2 == 20;

% Test Generator:
fprintf(generator_obj,'V1 1');
readData = query(generator_obj,'V1?');
readData = strsplit(readData(1:end-2), ' ');
test_gen_1 = str2double(readData{2});

fprintf(generator_obj,'V1 2');
readData = query(generator_obj,'V1?');
readData = strsplit(readData(1:end-2), ' ');
test_gen_2 = str2double(readData{2});

comStatus(2) = test_gen_1 == 1 && test_gen_2 == 2;

% Test Load:
fprintf(load_obj,'A 1');
readData = query(load_obj,'A?');
readData = strsplit(readData(1:end-2), ' ');
test_load_1 = str2double(readData{2});

fprintf(load_obj,'B 2');
readData = query(load_obj,'B?');
readData = strsplit(readData(1:end-2), ' ');
test_load_2 = str2double(readData{2});

comStatus(3) = test_load_1 == 1 && test_load_2 == 2;

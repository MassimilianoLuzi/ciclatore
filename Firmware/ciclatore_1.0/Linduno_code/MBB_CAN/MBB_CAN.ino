/*!
MBB_CAN
LTC6804-2: Battery stack monitor

@verbatim
NOTES
 Setup:
   Set the terminal baud rate to 115200 and select the newline terminator.
   Ensure all jumpers on the demo board are installed in their default positions from the factory.
   Refer to Demo Manual D1894B.


USER INPUT DATA FORMAT:
 decimal : 1024
 hex     : 0x400
 octal   : 02000  (leading 0)
 binary  : B10000000000
 float   : 1024.0

@endverbatim

http://www.linear.com/product/LTC6804-1

http://www.linear.com/product/LTC6804-1#demoboards

REVISION HISTORY
$Revision: 4432 $
$Date: 2015-11-30 14:03:02 -0800 (Mon, 30 Nov 2015) $

Copyright (c) 2013, Linear Technology Corp.(LTC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of Linear Technology Corp.

The Linear Technology Linduino is not affiliated with the official Arduino team.
However, the Linduino is only possible because of the Arduino team's commitment
to the open-source community.  Please, visit http://www.arduino.cc and
http://store.arduino.cc , and consider a purchase that will help fund their
ongoing work.

Copyright 2013 Linear Technology Corp. (LTC)
 */


/*! @file
    @ingroup LTC68042
*/
#define TOTAL_IC 1  //!<number of ICs in the isoSPI network LTC6804-2 ICs must be addressed in ascending order starting at 0.
#define TS 100       // Sampling time in milliseconds

#define RELE_PIN 4
#define CHECK_PIN 5
#define THRY_GEN_PIN 2
#define THRY_LOAD_PIN 3

#define CAN_CFG_RELE 0x400
#define CAN_CFG_RX_ID1 0x000
#define CAN_CFG_RX_ID2 0x001
#define CAN_CFG_RX_ID3 0x002
#define CAN_CFG_RX_ID4 0x003
#define CAN_CFG_TX_OS 0x010
#define CAN_V_ID_OS 0x0A0
#define CAN_AUX_ID_OS 0x100
 
#include <Arduino.h>
#include <stdint.h>
#include "Linduino.h"
#include "LT_SPI.h"
#include "LTC68042.h"
#include "mcp_can.h"
#include <SPI.h>

// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 9;

MCP_CAN CAN(SPI_CS_PIN);   // Set CS pin

unsigned long timer;
bool rxFlag = false;

/*!**********************************************************************
 \brief  Inititializes hardware and variables
 ***********************************************************************/
void setup()
{
  // Configure the rele
  pinMode(CHECK_PIN, INPUT);
  pinMode(RELE_PIN, OUTPUT);
  
  pinMode(THRY_GEN_PIN, OUTPUT);
  pinMode(THRY_LOAD_PIN, OUTPUT);
  
  digitalWrite(RELE_PIN, LOW);
  digitalWrite(THRY_GEN_PIN, LOW);
  digitalWrite(THRY_LOAD_PIN, LOW);

  // Configure LTC6804
  LTC6804_initialize();  //Initialize LTC6804 hardware
  set_adc(MD_FILTERED,DCP_ENABLED,CELL_CH_ALL,AUX_CH_GPIO3);
  init_cfg();           //initialize the 6804 configuration array to be written
  
  while (CAN_OK != CAN.begin(CAN_500KBPS))              // init can bus : baudrate = 500k
  {
    delay(100);
  }
  Serial.begin(115200);
  Serial.setTimeout(1);
  
  attachInterrupt(digitalPinToInterrupt(2), MCP2515_ISR, FALLING);

  CAN.init_Mask(0, 0, CAN_CFG_RELE);
  CAN.init_Mask(1, 0, CAN_CFG_RELE);
  CAN.init_Mask(0, 0, CAN_CFG_RX_ID1);
  CAN.init_Mask(1, 0, CAN_CFG_RX_ID1);
  CAN.init_Mask(0, 0, CAN_CFG_RX_ID2);
  CAN.init_Mask(1, 0, CAN_CFG_RX_ID2);
  CAN.init_Mask(0, 0, CAN_CFG_RX_ID3);
  CAN.init_Mask(1, 0, CAN_CFG_RX_ID3);
  CAN.init_Mask(0, 0, CAN_CFG_RX_ID4);
  CAN.init_Mask(1, 0, CAN_CFG_RX_ID4);

  timer = millis();
}

void MCP2515_ISR()
{
  rxFlag = true;
}

/*!*********************************************************************
  \brief main loop

***********************************************************************/
void loop()
{
  if (rxFlag)
  {
    rxFlag = false;
    ReadCommand();
  }
    
  if (millis() - timer >= TS)
  {
    timer = millis();

    PerformMeasures();
    SendCellsCan();
    SendAuxCan();
    SendConfigCan();
  }
}

/*!***********************************
 \brief Initializes the configuration array
 **************************************/
void init_cfg()
{
  uint8_t cfg[TOTAL_IC][6];
  /*!<
    The tx_cfg[][6] stores the LTC6804 configuration data that is going to be written
    to the LTC6804 ICs on the daisy chain. The LTC6804 configuration data that will be
    written should be stored in blocks of 6 bytes. The array should have the following format:
  
   |  tx_cfg[0][0]| tx_cfg[0][1] |  tx_cfg[0][2]|  tx_cfg[0][3]|  tx_cfg[0][4]|  tx_cfg[0][5]| tx_cfg[1][0] |  tx_cfg[1][1]|  tx_cfg[1][2]|  .....    |
   |--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|-----------|
   |IC1 CFGR0     |IC1 CFGR1     |IC1 CFGR2     |IC1 CFGR3     |IC1 CFGR4     |IC1 CFGR5     |IC2 CFGR0     |IC2 CFGR1     | IC2 CFGR2    |  .....    |
  
  */
  
  for (int i = 0; i<TOTAL_IC; i++)
  {
    cfg[i][0] = 0xFF ; // activate the second ad mode
    cfg[i][1] = 0x00 ;
    cfg[i][2] = 0x00 ;
    cfg[i][3] = 0x00 ;
    cfg[i][4] = 0x00 ;
    cfg[i][5] = 0x00 ;
  }
  
  wakeup_sleep();
  LTC6804_wrcfg(TOTAL_IC, cfg);
}

void PerformMeasures()
{
  wakeup_idle();
  LTC6804_adcv();
  delay(10);
  LTC6804_adax();
  delay(10);
}

void SendCellsCan()
{
  uint16_t canMsg[3*TOTAL_IC][4];
  int8_t error = 0;
  uint16_t cell_codes[TOTAL_IC][12];
  /*!<
    The cell codes will be stored in the cell_codes[][12] array in the following format:
  
    |  cell_codes[0][0]| cell_codes[0][1] |  cell_codes[0][2]|    .....     |  cell_codes[0][11]|  cell_codes[1][0] | cell_codes[1][1]|  .....   |
    |------------------|------------------|------------------|--------------|-------------------|-------------------|-----------------|----------|
    |IC1 Cell 1        |IC1 Cell 2        |IC1 Cell 3        |    .....     |  IC1 Cell 12      |IC2 Cell 1         |IC2 Cell 2       | .....    |
  ****/
  wakeup_idle();
  error = LTC6804_rdcv(0, TOTAL_IC, cell_codes);
  if (error == -1)
  {
    Serial.println("A PEC error was detected in the received data");
  }
  for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
  {
    for (int i=0; i<12; i++)
    {
      if(i<4)
      {
        canMsg[3*current_ic][i%4] = cell_codes[current_ic][i];
      }
      else if(i<8)
      {
        canMsg[3*current_ic + 1][i%4] = cell_codes[current_ic][i];
      }
      else
      {
        canMsg[3*current_ic + 2][i%4] = cell_codes[current_ic][i];
      }
    }
    CAN.sendMsgBuf(3*current_ic + CAN_V_ID_OS, 0, 8, (byte*)canMsg[3*current_ic]);
    CAN.sendMsgBuf(3*current_ic + CAN_V_ID_OS + 1, 0, 8, (byte*)canMsg[3*current_ic + 1]);
    CAN.sendMsgBuf(3*current_ic + CAN_V_ID_OS + 2, 0, 8, (byte*)canMsg[3*current_ic + 2]);
  }   
}

void SendAuxCan()
{
  uint16_t canMsg[2*TOTAL_IC][3]; 
  
  int8_t error = 0;
  uint16_t aux_codes[TOTAL_IC][6];
  /*!<
   The GPIO codes will be stored in the aux_codes[][6] array in the following format:
  
   |  aux_codes[0][0]| aux_codes[0][1] |  aux_codes[0][2]|  aux_codes[0][3]|  aux_codes[0][4]|  aux_codes[0][5]| aux_codes[1][0] |aux_codes[1][1]|  .....    |
   |-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|---------------|-----------|
   |IC1 GPIO1        |IC1 GPIO2        |IC1 GPIO3        |IC1 GPIO4        |IC1 GPIO5        |IC1 Vref2        |IC2 GPIO1        |IC2 GPIO2      |  .....    |
  */
  Serial.flush();
  
  wakeup_idle();
  error = LTC6804_rdaux(0, TOTAL_IC, aux_codes);
  if (error == -1)
  {
    Serial.println("A PEC error was detected in the received data");
  }
  
  for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
  {
    for (int i=0; i<6; i++)
    {
      if(i<3)
      {
        canMsg[2*current_ic][i%3] = aux_codes[current_ic][i];
      }
      else
      {
        canMsg[2*current_ic + 1][i%3] = aux_codes[current_ic][i];
      }
    } 
    CAN.sendMsgBuf(2*current_ic + CAN_AUX_ID_OS, 0, 6, (byte*)canMsg[2*current_ic]);
    CAN.sendMsgBuf(2*current_ic + CAN_AUX_ID_OS + 1, 0, 6, (byte*)canMsg[2*current_ic + 1]);
  }  
}
  
  void SendCellsSerial()
{
  int8_t error = 0;
  uint16_t cell_codes[TOTAL_IC][12];
  /*!<
    The cell codes will be stored in the cell_codes[][12] array in the following format:
  
    |  cell_codes[0][0]| cell_codes[0][1] |  cell_codes[0][2]|    .....     |  cell_codes[0][11]|  cell_codes[1][0] | cell_codes[1][1]|  .....   |
    |------------------|------------------|------------------|--------------|-------------------|-------------------|-----------------|----------|
    |IC1 Cell 1        |IC1 Cell 2        |IC1 Cell 3        |    .....     |  IC1 Cell 12      |IC2 Cell 1         |IC2 Cell 2       | .....    |
  ****/
  Serial.flush();
  wakeup_idle();
  error = LTC6804_rdcv(0, TOTAL_IC, cell_codes);
  if (error == -1)
  {
    Serial.println("A PEC error was detected in the received data");
  }
  for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
  {
    for (int i=0; i<12; i++)
    {
      Serial.print(cell_codes[current_ic][i]*0.0001, 4); Serial.print(',');
    }
  }   
  Serial.println();
}

void SendAuxSerial()
{
  int8_t error = 0;
  uint16_t aux_codes[TOTAL_IC][6];
  /*!<
   The GPIO codes will be stored in the aux_codes[][6] array in the following format:
  
   |  aux_codes[0][0]| aux_codes[0][1] |  aux_codes[0][2]|  aux_codes[0][3]|  aux_codes[0][4]|  aux_codes[0][5]| aux_codes[1][0] |aux_codes[1][1]|  .....    |
   |-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|---------------|-----------|
   |IC1 GPIO1        |IC1 GPIO2        |IC1 GPIO3        |IC1 GPIO4        |IC1 GPIO5        |IC1 Vref2        |IC2 GPIO1        |IC2 GPIO2      |  .....    |
  */
  Serial.flush();
  
  wakeup_idle();
  error = LTC6804_rdaux(0, TOTAL_IC, aux_codes);
  if (error == -1)
  {
    Serial.println("A PEC error was detected in the received data");
  }
  
  for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
  {
    Serial.print(aux_codes[0][0]*0.0001, 4);Serial.print(',');Serial.print(aux_codes[0][1]*0.0001, 4);Serial.print(',');Serial.print(aux_codes[0][2]*0.0001, 4);Serial.print(',');Serial.println();
  }  
}

/*!*****************************************************************
 \brief Prints the Configuration data that was read back from the
 LTC6804 to the serial port.
 *******************************************************************/
uint8_t ReadConfig(uint8_t cfg[][8])
{
  int8_t error = 0;
  /*!<
    the rx_cfg[][8] array stores the data that is read back from a LTC6804-1 daisy chain.
    The configuration data for each IC  is stored in blocks of 8 bytes. Below is an table illustrating the array organization:
  
  |rx_config[0][0]|rx_config[0][1]|rx_config[0][2]|rx_config[0][3]|rx_config[0][4]|rx_config[0][5]|rx_config[0][6]  |rx_config[0][7] |rx_config[1][0]|rx_config[1][1]|  .....    |
  |---------------|---------------|---------------|---------------|---------------|---------------|-----------------|----------------|---------------|---------------|-----------|
  |IC1 CFGR0      |IC1 CFGR1      |IC1 CFGR2      |IC1 CFGR3      |IC1 CFGR4      |IC1 CFGR5      |IC1 PEC High     |IC1 PEC Low     |IC2 CFGR0      |IC2 CFGR1      |  .....    |
  */
  wakeup_sleep();
  error = LTC6804_rdcfg(TOTAL_IC, cfg);
  if (error == -1)
  {
    Serial.println("A PEC error was detected in the received data");
  }
  return error;
}

void SendConfigCan()
{
  uint8_t error;
  uint8_t cfg[TOTAL_IC][8];

  error = ReadConfig(cfg);
  for (int current_ic=0; current_ic<TOTAL_IC; current_ic++)
  {
    CAN.sendMsgBuf(CAN_CFG_TX_OS+current_ic, 0, 8, (byte*)cfg[current_ic]);
  }
}

void ReadCommand()
{
  unsigned char len = 0;
  uint8_t msg[8];
  uint8_t cfg[TOTAL_IC][8], cfg2write[TOTAL_IC][6];
  /*!<
    The tx_cfg[][6] stores the LTC6804 configuration data that is going to be written
    to the LTC6804 ICs on the daisy chain. The LTC6804 configuration data that will be
    written should be stored in blocks of 6 bytes. The array should have the following format:
  
   |  tx_cfg[0][0]| tx_cfg[0][1] |  tx_cfg[0][2]|  tx_cfg[0][3]|  tx_cfg[0][4]|  tx_cfg[0][5]| tx_cfg[1][0] |  tx_cfg[1][1]|  tx_cfg[1][2]|  .....    |
   |--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|--------------|-----------|
   |IC1 CFGR0     |IC1 CFGR1     |IC1 CFGR2     |IC1 CFGR3     |IC1 CFGR4     |IC1 CFGR5     |IC2 CFGR0     |IC2 CFGR1     | IC2 CFGR2    |  .....    |
  
  */
  // iterate over all pending messages
  // If either the bus is saturated or the MCU is busy,
  // both RX buffers may be in use and reading a single
  // message does not clear the IRQ conditon.
  if (CAN_MSGAVAIL == CAN.checkReceive()) 
  {
    uint8_t error;
    uint8_t rele_status;
    unsigned int canId;

    bool LTC6804_flag = false;
    
    // read data,  len: data length, buf: data buf
    CAN.readMsgBuf(&len, msg);
    canId = CAN.getCanId();
    
    
    
    switch (canId)
    {
      case CAN_CFG_RELE:
        rele_status = msg[0];
        digitalWrite(RELE_PIN, rele_status);
        break;
        
      case CAN_CFG_RX_ID1:
        error = ReadConfig(cfg);
        LTC6804_flag = true;  
        for (int i = 0; i<TOTAL_IC && i<4; i++)
        {
          cfg2write[i][0] = cfg[i][0];
          cfg2write[i][1] = cfg[i][1];
          cfg2write[i][2] = cfg[i][2];
          cfg2write[i][3] = cfg[i][3];
          cfg2write[i][4] = msg[(2*i)%4];
          cfg2write[i][5] = msg[(2*i+1)%4];
        }
        break; 
  
      case CAN_CFG_RX_ID2:  
        error = ReadConfig(cfg);
        LTC6804_flag = true;
        for (int i = 0; i<TOTAL_IC && i<4; i++)
        {
          cfg2write[i+4][0] = cfg[i+4][0];
          cfg2write[i+4][1] = cfg[i+4][1];
          cfg2write[i+4][2] = cfg[i+4][2];
          cfg2write[i+4][3] = cfg[i+4][3];
          cfg2write[i+4][4] = msg[(2*(i+4))%4];
          cfg2write[i+4][5] = msg[(2*(i+4)+1)%4];
        }
        break; 
  
      case CAN_CFG_RX_ID3:  
        error = ReadConfig(cfg);
        LTC6804_flag = true;
        for (int i = 0; i<TOTAL_IC && i<4; i++)
        {
          cfg2write[i+8][0] = cfg[i+8][0];
          cfg2write[i+8][1] = cfg[i+8][1];
          cfg2write[i+8][2] = cfg[i+8][2];
          cfg2write[i+8][3] = cfg[i+8][3];
          cfg2write[i+8][4] = msg[(2*(i+8))%4];
          cfg2write[i+8][5] = msg[(2*(i+8)+1)%4];
        }
        break;
  
       case CAN_CFG_RX_ID4:  
        error = ReadConfig(cfg);
        LTC6804_flag = true;
        for (int i = 0; i<TOTAL_IC && i<4; i++)
        {
          cfg2write[i+12][0] = cfg[i+12][0];
          cfg2write[i+12][1] = cfg[i+12][1];
          cfg2write[i+12][2] = cfg[i+12][2];
          cfg2write[i+12][3] = cfg[i+12][3];
          cfg2write[i+12][4] = msg[(2*(i+12))%4];
          cfg2write[i+12][5] = msg[(2*(i+12)+1)%4];
        }
        break; 

       default:
         LTC6804_flag = true;
         for (int i = 0; i<TOTAL_IC; i++)
         { 
          cfg2write[i][0] = 0xFE;
          cfg2write[i][1] = 0x00;
          cfg2write[i][2] = 0x00;
          cfg2write[i][3] = 0x00;
          cfg2write[i][4] = 0x00;
          cfg2write[i][5] = 0x10;
         }
         break;
    }

    if (LTC6804_flag)
    {
      wakeup_sleep();
      LTC6804_wrcfg(TOTAL_IC, cfg2write);
    }
    
  }
}

void serialEvent()
{
  byte command = Serial.parseInt();
  
  switch(command)
  {
    case 0:
      digitalWrite(RELE_PIN, LOW);
      break;

    case 1:
      digitalWrite(RELE_PIN, HIGH);
      break;
    
    case 2:
      digitalWrite(THRY_GEN_PIN, LOW);
      digitalWrite(THRY_LOAD_PIN, LOW);
      break;
      
    case 3:
      digitalWrite(THRY_GEN_PIN, HIGH);
      digitalWrite(THRY_LOAD_PIN, LOW);
      break;
    
    case 4:
      digitalWrite(THRY_LOAD_PIN, HIGH);
      break;
    
    case 5:
      Serial.println("ACK");
      SendCellsSerial();
      SendAuxSerial();
      break;
      
    case 6:
      Serial.println("ACK");
      byte relayStatus;
      relayStatus = digitalRead(CHECK_PIN); 
      Serial.println(relayStatus);
      break;
    
    case 20:
      Serial.println(20);  
      break;
      
    case 21:
      byte releStatus[3];
      
      digitalWrite(RELE_PIN, LOW);
      delay(1000);
      releStatus[0] = digitalRead(CHECK_PIN); 
      
      digitalWrite(RELE_PIN, HIGH);
      delay(1000);
      releStatus[1] = digitalRead(CHECK_PIN);
 
      digitalWrite(RELE_PIN, LOW);
      delay(1000);
      releStatus[2] = digitalRead(CHECK_PIN);
      
      Serial.println((releStatus[0] || releStatus[1] || releStatus[2]));
      break;

    default:
      digitalWrite(RELE_PIN, LOW);
      digitalWrite(THRY_GEN_PIN, LOW);
      digitalWrite(THRY_LOAD_PIN, LOW);
  }
}


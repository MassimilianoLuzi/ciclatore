1. Installare Arduino IDE
2. Estrarre lo zip "LTSketchbook.zip"
	2.1 Copiare il contenuto della cartella "libraries" in "Documenti/Arduino"
3. Estrarre lo zip "CAN_BUS_Shield-master.zip"
	3.1 Copiare la cartella "CAN_BUS_Shield-master" in "Documenti/Arduino"
4. Aprire il progetto "MBB_CAN.ino" e caricarlo in Linduino One.